import numpy as np
import pandas as pd
import re

USE_DOCS = True

cnt = lambda l, words: sum(l.count(w) for w in words)

docs = pd.read_pickle('docs.pkl' if USE_DOCS else 'labeled_posts.pkl', compression='gzip')
docs['punc'] = docs['content'].apply(lambda c: re.findall(r"[^\w\s]+", c.lower(), re.UNICODE))
docs['words'] = docs['content'].apply(lambda c: re.findall(r"\w+", c.lower(), re.UNICODE))
docs['or_ratio'] = docs['words'].apply(lambda d: d.count('or')) / docs['wc']
docs['fpp_ratio'] = docs['words'].apply(lambda d: cnt(d, ['i','we','my','mine','our','ours'])) / docs['wc']
docs['tpp_ratio'] = docs['words'].apply(lambda d: cnt(d, ['he','she','it','him','her','his','hers','its','they','them','their','theirs'])) / docs['wc']
docs['but_ratio'] = docs['words'].apply(lambda d: d.count('but')) / docs['wc']
docs['token_length'] = docs['words'].apply(lambda d: sum(map(len, d))) / docs['wc']
docs['unique_tokens'] = docs['words'].apply(lambda d: len(set(d))) / docs['wc']
docs['message_count'] = docs['punc'].apply(lambda d: d.count('/!@') + 1)
docs['message_length'] = docs['wc'] / docs['message_count']
docs['sentence_length'] = docs['punc'].apply(lambda p: p.count('.') + p.count('!') + p.count('?')) / docs['wc']
docs['time'] = 1 + (docs['updated_at'] - docs['inserted_at']) / np.timedelta64(1, 'D')
docs['wc24h'] = docs['wc'] / docs['time']
docs['msg24h'] = docs['message_count'] / docs['time']

from sklearn.model_selection import cross_validate, StratifiedKFold
from sklearn import svm, ensemble, linear_model
from sklearn import dummy
from sklearn.naive_bayes import MultinomialNB
from sklearn.preprocessing import scale
from scipy.stats import spearmanr
from sklearn.metrics import make_scorer
import numpy

columns = ["wc24h", "or_ratio", "tpp_ratio", "fpp_ratio", "but_ratio", "token_length", "unique_tokens", "message_length", "sentence_length", "msg24h"]

if not USE_DOCS:
    columns.append("post_no")

X = docs[columns].values
scale(X, copy=False)

y = docs['scum'].values

def my_scorer(y, y_pred, **kwargs):
    print(docs.iloc[numpy.argmax(y_pred)])
    return 0

# model = dummy.DummyClassifier()
for c in np.logspace(-3, 2, 6):
    model = svm.LinearSVC(class_weight='balanced', C=c)
    scores = cross_validate(model, X, y, cv=StratifiedKFold(n_splits=10), scoring=['f1', 'accuracy', 'precision', 'recall', 'roc_auc', 'average_precision'])
    print(c, np.mean(scores['test_roc_auc']))
# cross_validate(model, X, y, cv=StratifiedKFold(), scoring=make_scorer(my_scorer))
for i, x in enumerate(X.T):
    print(columns[i], spearmanr(x, y))
