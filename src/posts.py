import pandas as pd
import re

post_files = ["mini-normal1.json", "mini-normal2.json", "large-normal.json"]
# post_files = ["large-normal.json"]
slot_files = ["mini-normal-slots.json", "large-normal-slots.json", "old-normal-slots.json"]

posts = pd.concat(pd.read_json(fn, orient='records') for fn in post_files)
slots = pd.concat(pd.read_json(fn, orient='records') for fn in slot_files)

posts.set_index(['game_id', 'author'], inplace=True)

# remove post-game discussion

first_posts = posts[posts["post_no"] == 0]

# moderator makes the first post
mod_posts = posts.merge(first_posts, left_index=True, right_index=True, suffixes=('', '_first'))

# last vote count is the last game post
# everything after that is post-game discussion and should be discarded
vote_counts = mod_posts[mod_posts["content"].str.contains("vote ?count|vc|not voting \(", case=False)]
last_game_posts = vote_counts['post_no'].max(level=0)

game_posts = posts.join(last_game_posts, rsuffix='_last', how='inner')
game_posts = game_posts[game_posts['post_no'] <= game_posts['post_no_last']]

first_player_posts = game_posts[game_posts["post_no"] == 2]
# game_lengths = first_player_posts["inserted_at_last"] - first_player_posts["inserted_at"]
# print(game_lengths)
# print("average game length:", game_lengths.mean())

# concatenate all posts per player per game
# docs = game_posts.sort_values('post_no').groupby(level=[0,1], sort=False)
# docs = docs['content'].apply(lambda x: ' /!@ '.join(x)).to_frame().reset_index(level=['game_id','author'])

users = []

slots['scum'] = slots['role'].str.contains("mafia|goon|wolf", case=False)
slots['town'] = slots['role'].str.contains("town", case=False)
slots['sk'] = slots['role'].str.contains("serial.?killer", case=False) | slots['role'].str.contains("SK")
slots = slots[~(slots['town'] & slots['scum']) & pd.notnull(slots['scum']) & ~slots['sk']]
slots = slots.groupby('game_id').filter(lambda g: any(g['scum']))

# slots.set_index('game_id', inplace=True)

users = pd.DataFrame({'game_id': slot['game_id'], 'author': user, 'scum': slot['scum']} for i, slot in slots.iterrows() for user in slot['users'])

game_posts.reset_index(inplace=True)

labeled_posts = pd.merge(game_posts, users, on=['game_id','author'], how='inner')
labeled_posts['wc'] = labeled_posts['content'].apply(lambda c: len(re.findall(r"\w+", c.lower(), re.UNICODE)))
labeled_posts = labeled_posts[labeled_posts['wc'] > 5]
print(labeled_posts['game_id'].nunique(), "games,", len(labeled_posts), "posts")

labeled_posts.to_pickle('labeled_posts.pkl', compression='gzip')
