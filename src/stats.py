import pandas as pd
import matplotlib as mpl
import numpy as np
from scipy.stats import spearmanr
mpl.use('Agg')

docs = pd.read_pickle('docs.pkl', compression='gzip')
games = docs.groupby('game_id')
g = games['slot_id'].nunique().to_frame('size')
g['start'] = games['inserted_at'].min()
g['starting_year'] = g['start'].dt.year
g['length'] = (games['updated_at'].max() - g['start']) / np.timedelta64(1, 'M')
g['scum_ratio'] = games['scum'].sum() / games.size()
games_played = docs['author'].value_counts()

wc = docs['wc'].plot.hist(x=0, bins=np.logspace(1.0, 5.0, 50), logx=True)
not_replaced = docs.drop_duplicates(['slot_id'], keep=False)['wc'].plot.hist(x=0, bins=np.logspace(1.0, 5.0, 50), logx=True)
wc.set_xlabel("Word Count")
wc.legend(["All documents", "Documents from slots without replacements"], loc='upper center', bbox_to_anchor=(0.5,1.16))
# wc.get_figure().savefig('wc_hist')

mpl.pyplot.figure()

p = games_played.plot.hist(bins=50)
p.set_xlabel("Games Played")
# p.get_figure().savefig('games_played_hist')

mpl.pyplot.figure()

game_dist = games_played.value_counts() * games_played.value_counts().index
for i in range(1, game_dist.index.max()):
    if not i in game_dist:
        game_dist[i] = 0
game_dist.sort_index(inplace=True)
d = game_dist.plot(kind='bar', figsize=(10,6))
d.set_xlabel("Accounts that have played in ___ games...")
d.set_ylabel("...produced ___ of all documents")
# d.get_figure().savefig('hist2')
print(game_dist)


l = g['length'].plot.hist(bins=20)
l.set_xlabel("Game Length in Months")
# l.get_figure().savefig("game_length")

# possible confounders

mpl.pyplot.figure()

print(g[g['scum_ratio']>0.4])

s = g[g['size']>8].plot(x='size', y='scum_ratio', kind='scatter')
s.set_xlabel('Game Size')
s.set_ylabel('Mafia ratio')
print(spearmanr(g['size'].values, g['scum_ratio'].values))
# s.get_figure().savefig("size_scum_ratio")

mpl.pyplot.figure()

s = g.plot(x='starting_year', y='scum_ratio', kind='scatter')
s.set_xlabel('Game Start')
s.set_ylabel('Mafia ratio')
print(spearmanr(g['starting_year'].values, g['scum_ratio'].values))
# s.get_figure().savefig("start_scum_ratio")

print(docs.columns)

res = pd.read_pickle('res-manywc.pkl')
res['ROC AUC'] = res['scores'].apply(lambda s: np.mean(s['test_roc_auc']))
res['PR AUC'] = res['scores'].apply(lambda s: np.mean(s['test_average_precision']))
res[res['replacements']].plot('wc', 'ROC AUC')
