import numpy as np
import delong
import pandas as pd
import re
import matplotlib as mpl
# mpl.use('Agg')
import matplotlib.pyplot as plt

USE_DOCS = True
USE_CACHED_DOCS = True

wv = [("FastText", "wiki"), ("GloVe", "wiki")]
# wv = [("GloVe", "wiki200d"), ("GloVe", "twitter")]

cnt = lambda l, words: sum(l.count(w) for w in words)
cnt_phrases = lambda l, phrases: sum(l[i:i+len(p)]==p for p in phrases for i in range(len(l)))
contains_any = lambda l, words: sum(any(punc.find(w) != -1 for w in words) for punc in l)
not_contractions = [c.split("'") for c in "can't|couldn't|didn't|doesn't|don't|hadn't|hasn't|haven't|isn't|weren't|wasn't|shouldn't|aren't|won't".split("|")]
other_negations = "cannot|neither|never|no|nobody|none|nope|nor|not|nothing|nowhere|uhuh|without|wouldn|zero|zip".split("|")
neg_emotion = re.compile(r"^(abandon.*|abuse.*|abusive|ache.*|aching|advers.*|afraid|aggravat.*|aggress.*|agitat.*|agony|alarm.*|alone|anger.*|angr.*|anguish.*|annoy.*|antagoni.*|anxi.*|appall.*|apprehens.*|argu.*|arrogan.*|asham.*|assault.*|aversi.*|avoid.*|awful|bad|bastard|beaten|bewilder.*|bitch.*|bitter.*|blam.*|bore.*|boring|bother.*|burden.*|careless.*|cheat.*|complain.*|confus.*|contradic.*|crap.*|craz.*|cried|cries|critical|critici.*|cruel.*|crushed|cry|crying|cut|cynical|damn.*|danger.*|daze.*|decay.*|defeat.*|defect.*|defens.*|degrad.*|depress.*|depriv.*|despair.*|desperate.*|despis.*|destroy.*|destruct.*|devastat.*|devil.*|difficult.*|disagree.*|disappoint.*|disaster.*|discomfort.*|discourag.*|disgust.*|dislike|disliked|dislikes|dismay.*|distraught|distress.*|distrust.*|disturb.*|dominate.*|doom.*|doubt.*|dread.*|dull.*|dumb.*|dump.*|dwell.*|egotis.*|embarass.*|emotional|empt.*|enem.*|enrag.*|envious|envy|evil|excruciat.*|exhaust.*|fail.*|fatal|fatigu.*|fear|feared|fearing|fears|feud.*|fight|fighting|fights|flop.*|flunk.*|forbid.*|fought|frantic.*|freak.*|fright.*|frustrat.*|fuck.*|furious.*|gloom.*|goddam.*|gossip.*|grave.*|greed.*|grief|griev.*|grim.*|grind|gross.*|guilt.*|harass.*|hate|hated|hateful|hates|hating|hatred|hazy|hell|helpless.*|hesitant|homesick.*|hopeless.*|horribl.*|horrif.*|horror|hostil.*|humiliat.*|hurt.*|ignoran.*|impatien.*|impersonal|inadequate|indifferen.*|ineffect.*|inferior|inhib.*|insecur.*|insult.*|interrup.*|intimidat.*|irrational|irrita.*|isolat.*|jealous.*|jerk|jerked|jerks|kill.*|lame|liar.*|lie|lied|lies|loneli.*|lonely|lonesome|longing|lose|loser.*|losing|loss.*|lost|lous.*|low.*|ludicrous.*|mad|mess|messy|miser.*|miss|missed|misses|missing|molest.*|moody|mourn.*|nag.*|nast.*|neglect.*|nervous.*|numb|obnoxious.*|obsess.*|offend.*|outrag.*|overwhelm.*|pain|painf.*|painl.*|pains|panic.*|paranoi.*|pathetic.*|peculiar.*|pervert.*|pessimis.*|petrif.*|pett.*|piss.*|pitiful.*|pity|poison.*|prejudic.*|pressur.*|protest|protested|protesting|puk.*|punish.*|rage.*|rape.*|rebel.*|regret.*|reject.*|reluctan.*|remorse.*|repress.*|resent.*|resign.*|restless.*|revenge.*|ridicul.*|rigid.*|rude.*|ruin.*|sad|sarcas.*|scare.*|scream.*|screw.*|selfish.*|serious.*|severe.*|shak.*|shame.*|shit.*|shock.*|shy.*|sicken.*|silly|sin|sinister|sins|skeptical|smother.*|snob.*|sorrow.*|sorry|spite.*|startl.*|strain.*|strange|stress.*|stubborn.*|stunned|stuns|stupid|suck|sucked|sucking|sucks|suffer|suffered|suffering|suffers|suspicious.*|tear.*|teas.*|temper|tense.*|tension.*|terribl.*|terrified.*|terrifying|terror.*|threaten.*|tick|ticked|torture.*|tragedy|tragic|trembl.*|trick.*|troubl.*|turmoil|ugh|ugly|unattractive|uncertain|uncomfortable|uneas.*|unfortunate.*|unhapp.*|unimportant|unpleasant|unprotected|unsuccessful|unsure.*|upset.*|useless|vain|vanity|vicious.*|victim.*|violent.*|vulnerab.*|weak.*|weep.*|weird.*|whine.*|wicked.*|worr.*|worse.*|worthless|wrong.*)$")
sensory = re.compile(r"^(appear|appeared|appearing|appears|ask|asked|asking|asks|ate|bitter*|call|called|calling|calls|chat.*|contact.*|discuss.*|drank|drink|drinking|drinks|ear|ears|eat|eaten|eating|eats|eye.*|feel|feeling.*|feels|felt|grab.*|handl.*|hear|heard|hearing|hears|held|hold|holding|holds|hug.*|itch.*|listen|listened|listening|listens|look|looked|looking|looks|noise.*|observ.*|pain|painf.*|painl.*|pains|perceiv.*|perception.*|read|reading|reads|rub|rubbed|rubs|said|saw|say.*|see|seeing|seen|sees|sensation|sensations|sense|sensed|senses|sensing|show|showed|showing|shows|sight.*|skin|smell.*|sound.*|speak|speaking|speaks|spoke.*|squeez.*|stare.*|sweet|talk|talked|talking|talks|tast.*|tell|telling|tells|told|touch.*|view|viewed|viewing|views|vision.*|visual|watch*|witness)$")
anger = re.compile(r"^(abuse.*|abusive|aggravat.*|aggress.*|agitat.*|anger.*|angr.*|annoy.*|antagoni.*|argu.*|arrogan.*|assault.*|bastard|beaten|bitch.*|bitter.*|blam.*|cheat.*|contradic.*|crap.*|critical|critici.*|cruel.*|cut|cynical|damn.*|danger.*|defens.*|despis.*|destroy.*|destruct.*|disgust.*|distrust.*|dominate.*|dread.*|dumb.*|dump.*|enem.*|enrag.*|evil|feud.*|fight|fighting|fights|fought|frustrat.*|fuck.*|furious.*|goddam.*|greed.*|harass.*|hate|hated|hateful|hates|hating|hatred|hostil.*|humiliat.*|insult.*|interrup.*|intimidat.*|jealous.*|jerk|jerked|jerks|kill.*|liar.*|lied|lous.*|ludicrous.*|mad|molest.*|nag.*|nast.*|obnoxious.*|offend.*|outrag.*|paranoi.*|piss.*|poison.*|prejudic.*|punish.*|rage.*|rape.*|rebel.*|resent.*|revenge.*|ridicul.*|rude.*|sarcas.*|screw.*|shit.*|sinister|skeptical|smother.*|snob.*|spite.*|stubborn.*|stupid|suck|sucked|sucking|sucks|suspicious.*|teas.*|temper|terrified.*|terrifying|terror.*|threaten.*|tick|ticked|torture.*|trick.*|ugly|vicious.*|victim.*|violent.*|wicked)$")

spp = "thee thine thou thoust thy yall ya ye you your".split()
quantifiers = 'all some much many little few any lots lot plenty several more most less least none'.split()
insight = re.compile(r"^(accept|acknowledg.*|adjust.*|admit|admits|admitted|admitting|analys.*|analyz.*|answer.*|aware.*|became|believe|believed|believes|believing|clarif.*|clear|closure|cohere.*|compreh.*|concentrat.*|conclud.*|conclus.*|confess.*|construct.*|create.*|creating|decid.*|determina.*|determine|determined|determines|determining|discern.*|discl.*|discover.*|effect.*|enlighten.*|evaluat.*|examine.*|examining|explain|explained|explaining|explains|explanat.*|explor.*|feeling.*|feels|felt|figur.*|find.*|forgiv.*|found|gather.*|generate.*|grasp.*|inform|informs|insight.*|knew|know|knowing|knowl.*|known|knows|learn.*|meaning|meaningf.*|means|meant|motivate.*|perceiv.*|perception.*|ponder.*|question|questioning|questionned|questions|rational.*|realiz.*|reason.*|reckon.*|recognis.*|recogniz.*|reconsider.*|reconstruct.*|reflect.*|relate.*|relation.*|resolu.*|resolve|resolved|rethink.*|reveal.*|saw|secret|secrets|see|seeing|solution.*|solve.*|suspect.*|think|thinking|thinks|thought|thoughts|understand|understandable|understanding|understands|understood|wonder|wondered|wondering)$")

cognitive = re.compile(r"abandon.*|accept|accepted|accepting|accepts|achiev.*|acknowledg.*|adjust.*|admit|admits|admitted|admitting|affect|affected|affects|agree.*|anal|analys.*|analyz.*|answer.*|approv.*|arrange.*|assum.*|avoid.*|aware.*|barrier.*|bases|basis|became|because|become|becomes|becoming|believe|believed|believes|believing|block.*|brake.*|but|careful.*|caus.*|clarif.*|clear|clog.*|closure|cohere.*|complete|compreh.*|concentrat.*|concern.*|conclud.*|conclus.*|confess.*|confide|confided|confides|confiding|confirm.*|conflict.*|confus.*|consequen.*|constrain.*|constrict.*|construct.*|contain.*|contradic.*|control.*|cos|could|could'.*|couldn't|coz|create.*|creating|cuz|decid.*|defens.*|delay.*|deni.*|deny.*|depend|depended|depending|depends|describe|described|describes|describing|determina.*|determine|determined|determines|determining|digest.*|discern.*|discl.*|discover.*|disregard.*|done|doubt.*|duties|duty|effect.*|end|ended|ending.*|ends|enlighten.*|evaluat.*|examine.*|examining|expect.*|explain|explained|explaining|explains|explanat.*|explor.*|fact.*|feeling.*|feels|felt|figur.*|find.*|finish|fit|fits|forbid.*|forgiv.*|found|foundation.*|gather.*|generate.*|goal.*|grasp.*|guard.*|held|hence|hesitant|hesitat.*|hold|holding|holds|hope|hoped|hopef.*|hopes|hoping|how|how's|if|ignore.*|ignori.*|implic.*|incorporat.*|induc.*|infer|inferred|inferring|infers|influenc.*|inform|informs|inhib.*|initiat.*|insight.*|integrat.*|intell.*|interfer.*|justif.*|kind_of|kinda|knew|know|knowing|knowl.*|known|knows|learn.*|limit.*|meaning|meaningf.*|means|meant|mind.*|motivate.*|motive.*|must|need|needed|needing|needs|neglect.*|obstac.*|organize.*|organizing|origin|ought|outcome.*|perceiv.*|perception.*|ponder.*|pretty|prevent.*|produce.*|product|productive.*|prohib.*|purpose.*|question|questioning|questionned|questions|quit.*|rational.*|react.*|read|reading|reads|realiz.*|reason.*|reckon.*|recognis.*|recogniz.*|reconsider.*|reconstruct.*|reflect.*|refrain|refus.*|relate.*|relation.*|reluctan.*|remember.*|repress.*|require|required|requirement.*|requires|resolu.*|resolve|resolved|responsib.*|restrain.*|restrict.*|result.*|retard.*|rethink.*|reveal.*|rigid.*|root.*|saw|secret|secrets|see|seeing|seem|seemed|seems|settl.*|should|should'.*|shouldn't|since|smart.*|solution.*|solve.*|sort|sorta|source.*|stimul.*|stop|stopped|stopping|stops|structure.*|stubborn.*|suspect.*|therefor.*|think|thinking|thinks|thought|thoughts|thus|tried|tries|try|trying|understand|understandable|understanding|understands|understood|undo|unresolve.*|wait|waited|waiting|waits|want|wanted|wanting|wants|welcom.*|what|what's|why|why's|wish|wished|wishes|wishing|withheld|withhold|wonder|wondered|wondering|would|would'.*|wouldn'.*|yield.*")

motion= re.compile(r"^(action.*|advanc.*|approach|arrive|arrived|arrives|arriving|bring|bringing|brings|brought|carried|carries|carry|carrying|climb.*|cross|cruis.*|danc.*|deliver.*|depart|departs|disappear.*|drift.*|drive|driven|drives|driving|drove|enter|entered|entering|enters|explor.*|fled|flew|flies|fly|flying|follow|followed|following|follows|go|goes|going|gone|hik.*|jog.*|move|moved|moves|moving|pack|packed|packing|ran|run|running|runs|swim.*|take|takes|taking|took|transport.*|travel.*|visit.*|walk|walked|walking|walks|went)$")

tentative = re.compile(r"^(		alot|ambigu.*|any|anybod.*|anyhow|anyone.*|anything.*|anytime|anywhere|bet|bets|betting|conflict.*|confus.*|depend|depended|depending|depends|disorient.*|doubt.*|even|fortunat.*|fuzzy|guess|guessed|guesses|guessing|hazy|hesitant|hesitat.*|hope|hoped|hopef.*|hopes|hoping|just|kind_of|kinda|likel.*|luck|lucki.*|lucky|may|maybe|might|nearly|occasional.*|or|perhaps|possib.*|pretty|probab.*|puzzl.*|random|reckon.*|seem|seemed|seems|some|somebod.*|someone.*|something.*|sometime.*|sort|sorta|suppose|supposed|temporar.*|tentative.*|uncertain|unclear|undecided.*|unknow.*|unlikely|unresolve.*|unsure.*|vague.*|variable.*|wonder|wondered|wondering)$")

greeting = re.compile(r"\b(hey|hello|hi|howdy)\s+(guys|folks|all|everyone|everybody)\b")
greeting = re.compile(r"\b(hey|hello|hi|howdy)\b")

if USE_CACHED_DOCS:
    docs = pd.read_pickle('docs.pkl', compression='gzip')
    print('gr')
    firstpost = docs['content'].str.split('/!@').str[0]
    docs['greeting'] = firstpost.str.contains(greeting)

    og = docs.sort_values('inserted_at').drop_duplicates(['slot_id'])

    print("loaded")
else:
    post_files = ["mini-normal1.json", "mini-normal2.json", "large-normal.json"]
    slot_files = ["mini-normal-slots.json", "large-normal-slots.json", "old-normal-slots.json"]

    posts = pd.concat(pd.read_json(fn, orient='records') for fn in post_files)
    slots = pd.concat(pd.read_json(fn, orient='records') for fn in slot_files)

    posts.set_index(['game_id', 'author'], inplace=True)

    # remove post-game discussion

    first_posts = posts[posts["post_no"] == 0]

    # moderator makes the first post
    mod_posts = posts.loc[first_posts.index]

    # last vote count is the last game post
    # everything after that is post-game discussion and should be discarded
    vote_counts = mod_posts[mod_posts["content"].str.contains("vote ?count|vc|not voting \(", case=False)]
    last_game_posts = vote_counts['post_no'].max(level=0)

    game_posts = posts.join(last_game_posts, rsuffix='_last', how='inner')
    game_posts = game_posts[game_posts['post_no'] <= game_posts['post_no_last']]

    first_player_posts = game_posts[game_posts["post_no"] == 2]
    # game_lengths = first_player_posts["inserted_at_last"] - first_player_posts["inserted_at"]
    # print(game_lengths)
    # print("average game length:", game_lengths.mean())

    # concatenate all posts per player per game

    post_elim_time = np.timedelta64(1, 'D')
    groups = game_posts.sort_values('post_no').groupby(level=[0,1], sort=False)
    # join all posts to a document; filter out the last 24h of each document
    docs = groups.apply(lambda g: ' /!@ '.join(g[g['inserted_at'] < g['inserted_at'].max() - post_elim_time]['content'])).to_frame(name='content')
    docs['inserted_at'] = groups['inserted_at'].min()
    docs['updated_at'] = groups['inserted_at'].max()
    docs.reset_index(inplace=True)

    slots['scum'] = slots['role'].str.contains("mafia|goon|wolf|serial.?killer", case=False) | slots['role'].str.contains("SK")
    slots['town'] = slots['role'].str.contains("town", case=False)
    slots['sk'] = slots['role'].str.contains("serial.?killer", case=False) | slots['role'].str.contains("SK")

    # filters incomplete alignment distributions
    # use this when calculating game size/scum ratio correlations
    # slots = slots.groupby('game_id').filter(lambda g: all(pd.notnull(g['scum'])))

    slots = slots[~(slots['town'] & slots['scum']) & ~slots['sk']]
    slots = slots.groupby('game_id').filter(lambda g: any(g['scum']))

    slots['replacements'] = slots['users'].map(lambda x: len(x) - 1)

    slots.reset_index(inplace=True)
    users = pd.DataFrame({'game_id': slot['game_id'], 'author': user, 'scum': slot['scum'], 'slot_id': i, 'event': slot['event']} for i, slot in slots.iterrows() for user in slot['users'])

    docs = pd.merge(docs, users, on=['game_id','author'], how='inner')

    docs['words'] = docs['content'].apply(lambda c: re.findall(r"\w+", c.lower(), re.UNICODE))
    docs['wc'] = docs['words'].map(len)

    print("UNFILTERED:")
    print(slots.groupby('scum').mean())
    print(docs['game_id'].nunique(), "games,", docs['slot_id'].nunique(), "slots,", len(docs), "documents")
    print("scum ratio = ", sum(docs['scum']) / len(docs))

    docs = docs[docs['wc'] >= 50]

    print("\nWC >= 50:")
    print(slots.groupby('scum').mean())
    print(docs['game_id'].nunique(), "games,", docs['slot_id'].nunique(), "slots,", len(docs), "documents")
    print("scum ratio = ", sum(docs['scum']) / len(docs))

    cnt = lambda l, words: sum(l.count(w) for w in words)

    docs['punc'] = docs['content'].apply(lambda c: re.findall(r"[^\w\s]+", c.lower(), re.UNICODE))
    docs['or_ratio'] = docs['words'].apply(lambda d: d.count('or')) / docs['wc']
    docs['neg_em_ratio'] = docs['words'].apply(lambda d: sum(re.match(neg_emotion, word) != None for word in d)) / docs['wc']
    docs['anger_ratio'] = docs['words'].apply(lambda d: sum(re.match(anger, word) != None for word in d)) / docs['wc']
    docs['sensory_ratio'] = docs['words'].apply(lambda d: sum(re.match(sensory, word) != None for word in d)) / docs['wc']
    docs['cog_ratio'] = docs['words'].apply(lambda d: sum(re.match(cognitive, word) != None for word in d)) / docs['wc']
    docs['insight_ratio'] = docs['words'].apply(lambda d: sum(re.match(insight, word) != None for word in d)) / docs['wc']
    docs['motion_ratio'] = docs['words'].apply(lambda d: sum(re.match(motion, word) != None for word in d)) / docs['wc']
    docs['tent_ratio'] = docs['words'].apply(lambda d: sum(re.match(tentative, word) != None for word in d)) / docs['wc']
    print("regex done")
    docs['not_ratio'] = docs['words'].apply(lambda d: cnt_phrases(d, not_contractions) + cnt(d, other_negations)) / docs['wc']
    docs['fpp_ratio'] = docs['words'].apply(lambda d: cnt(d, ['i','we','my','mine','our','ours', 'myself', 'ourselves'])) / docs['wc']
    docs['spp_ratio'] = docs['words'].apply(lambda d: cnt(d, spp)) / docs['wc']
    docs['tpp_ratio'] = docs['words'].apply(lambda d: cnt(d, ['he','she','him','her','his','hers','they','them','their','theirs','themselves','himself','herself'])) / docs['wc']
    docs['quant_ratio'] = docs['words'].apply(lambda d: cnt(d, quantifiers)) / docs['wc']
    docs['sentence_length'] = docs['punc'].apply(lambda d: contains_any(d, ['.','!','?'])) / docs['wc']
    print("did things")
    docs['but_ratio'] = docs['words'].apply(lambda d: d.count('but')) / docs['wc']
    docs['token_length'] = docs['words'].apply(lambda d: sum(map(len, d))) / docs['wc']
    docs['unique_tokens'] = docs['words'].apply(lambda d: len(set(d))) / docs['wc']
    docs['message_count'] = docs['punc'].apply(lambda d: d.count('/!@') + 1)
    docs['message_length'] = docs['wc'] / docs['message_count']
    docs['sentence_length'] = sum(docs['punc'].str.contains(r"\.|!|?")) / docs['wc']
    docs['time'] = 1 + (docs['updated_at'] - docs['inserted_at']) / np.timedelta64(1, 'D')
    docs['wc24h'] = docs['wc'] / docs['time']
    docs['msg24h'] = docs['message_count'] / docs['time']

    # python -m gensim.scripts.glove2word2vec -i glove.twitter.27B.200d.txt -o w2v.twitter.txt

    from gensim.models.wrappers import FastText
    from gensim.models import KeyedVectors

    for m, dataset in wv:
        x = FastText.load_fasttext_format(dataset+".en") if m == "FastText" else KeyedVectors.load_word2vec_format('w2v.'+dataset+'.txt', binary=False)

        total_count = missing_count = 0

        def V(w):
            global total_count, missing_count
            try:
                total_count += 1
                return x[w]
            except KeyError:
                missing_count += 1
                return None

        # exclude numbers since pre-trained model does not have ngrams for them
        docs['vector_' + m + '_' + dataset] = docs['words'].apply(lambda ws: np.mean([V(word) for word in ws if not V(word) is None], axis=0))

        print("attempted to convert", total_count, "words to word embeddings, failed on", missing_count,
            "\nsuccess ratio =", (total_count - missing_count) / total_count)

        # if USE_DOCS:
        #     docs.to_pickle('fasttext-'+dataset+'-avg.pkl' if FASTTEXT else 'glove-'+dataset+'-avg.pkl', compression='gzip')
        # else:
        #     docs.to_pickle('fasttext-'+dataset+'-avg-posts.pkl' if FASTTEXT else 'glove-'+dataset+'-avg-posts.pkl', compression='gzip')

all_docs = docs

print(docs.groupby('scum').mean())
print(docs['game_id'].nunique(), "games,", docs['slot_id'].nunique(), "slots,", len(docs), "documents")
print("scum ratio = ", sum(docs['scum']) / len(docs))

from sklearn.model_selection import cross_validate, StratifiedKFold, RepeatedStratifiedKFold
from sklearn import svm, linear_model
from sklearn import dummy
from sklearn.naive_bayes import MultinomialNB
from sklearn.preprocessing import scale
# from scipy.stats import spearmanr
import scipy
import scipy.stats
from sklearn.metrics import make_scorer, roc_curve, precision_recall_curve, average_precision_score, accuracy_score, roc_auc_score

columns = "wc24h or_ratio tpp_ratio spp_ratio fpp_ratio but_ratio token_length unique_tokens message_length sentence_length msg24h anger_ratio sensory_ratio cog_ratio insight_ratio motion_ratio not_ratio quant_ratio neg_em_ratio tent_ratio".split()


docs = docs.drop_duplicates(['slot_id'], keep=False)
X = docs[columns].values
scale(X, copy=False)

y = docs['scum'].values

for i, x in enumerate(X.T):
    print(columns[i])
    plt.figure()
    docs[columns[i]].hist()
    u, p = scipy.stats.mannwhitneyu(x[y], x[~y])
    r = roc_auc_score(y, x)
    r = str(round(r, 3))
    c = columns[i].replace('_', ' ')
    print(r'\textbf{' + c + '}' if p < 0.05 else c, "&", r'\textbf{' + r + '}' if p < 0.05 else r, r'\tabularnewline')

scoring = {s:s for s in ['f1', 'accuracy', 'precision', 'recall', 'roc_auc', 'average_precision']}
curves = []
curves_i = -1

def roc_curve_as_score(y, y_pred, **kwargs):
    global curves, curves_i
    curves_i += 1
    curves.append(roc_curve(y, y_pred))
    # print(curves[curves_i][2])
    return curves_i

def pr_curve(y, y_pred, **kwargs):
    global curves, curves_i
    curves_i += 1
    curves.append(precision_recall_curve(y, y_pred))
    return curves_i

scoring['roc_curve'] = make_scorer(roc_curve_as_score, needs_threshold=True)
scoring['pr_curve'] = make_scorer(pr_curve, needs_threshold=True)

## Evaluation

res = pd.DataFrame(columns=['name', 'wc', 'N', 'replacements', 'scores'])
wc = -1000

def evaluate(X, y, weights, res, name):
    cv = RepeatedStratifiedKFold(n_splits=20, n_repeats=1)
    for train_index, test_index in cv.split(X, y):
        # model = svm.LinearSVC(class_weight='balanced', max_iter=10000)
        model = linear_model.LogisticRegression(class_weight='balanced', solver='lbfgs', max_iter=1000)
        model.fit(X[train_index], y[train_index], weights[train_index])
        y_pred[test_index] = model.predict_proba(X[test_index])[:,1]

    scores = {'test_average_precision': average_precision_score(y, y_pred), 'test_roc_auc': roc_auc_score(y, y_pred), 'interval': delong.interval(y, y_pred)}
    res = res.append({'name': name, 'wc': 'all', 'N': N, 'replacements': replacements, 'scores': scores}, ignore_index=True)

    c = docs['wc'] > 5000

    scores = {'test_average_precision': average_precision_score(y[c], y_pred[c]), 'test_roc_auc': roc_auc_score(y[c], y_pred[c]), 'interval': delong.interval(y[c], y_pred[c])}
    res = res.append({'name': name, 'wc': '5000+', 'N': sum(c), 'replacements': replacements, 'scores': scores}, ignore_index=True)

    for wc_cohort in range(wc, 11000, 2000):
        cohort = (docs['wc'] >= wc_cohort) & (docs['wc'] < wc_cohort + 2000)
        c = cohort.values
        scores = {'test_average_precision': average_precision_score(y[c], y_pred[c]), 'test_roc_auc': roc_auc_score(y[c], y_pred[c]), 'interval': delong.interval(y[c], y_pred[c])}
        res = res.append({'name': name, 'wc': wc_cohort, 'N': sum(c), 'replacements': replacements, 'scores': scores}, ignore_index=True)
    c = docs['wc'] > 11000

    scores = {'test_average_precision': average_precision_score(y[c], y_pred[c]), 'test_roc_auc': roc_auc_score(y[c], y_pred[c]), 'interval': delong.interval(y[c], y_pred[c])}
    res = res.append({'name': name, 'wc': '11000+', 'N': sum(c), 'replacements': replacements, 'scores': scores}, ignore_index=True)
    return res


for replacements in [True]:
    # for wc in [5000]:
    docs = all_docs[all_docs['wc'] >= wc]
    print("CH")
    print(sum(docs['scum'])/len(docs))

    if not replacements:
        docs = docs.drop_duplicates(['slot_id'], keep=False)

    y = docs['scum'].values

    N = len(docs)

    X_hp = docs[columns].values
    scale(X_hp, copy=False)

    def my_scorer(y, y_pred, **kwargs):
        print(docs.iloc[np.argmax(y_pred)])
        return 0

    y_pred = np.zeros(len(y))

    # model = dummy.DummyClassifier()
    print('hey')
    # scores = cross_validate(model, X, y, cv=cv, scoring=scoring, return_train_score=False, return_estimator=True)

    res = evaluate(X_hp, y, np.log(docs['wc'].values), res, 'Hand-picked')

    # cross_validate(model, X, y, cv=StratifiedKFold(), scoring=make_scorer(my_scorer))

    # rocs = np.array(curves)[scores['test_roc_curve']]
    # tprs = []
    # mean_fpr = np.linspace(0, 1, 100)
    # plt.plot([0, 1], [0, 1], linestyle='--', lw=2, color='r', label='Chance', alpha=.8)
    # for fpr, tpr, _ in rocs:
    #     plt.plot(fpr, tpr, alpha=0.3)
    #     tprs.append(scipy.interp(mean_fpr, fpr, tpr))
    #     tprs[-1][0] = 0.0
    #     tprs[-1][-1] = 1.0
    # plt.plot(mean_fpr, np.mean(tprs, axis=0), alpha=.8, label='Mean ROC curve')
    # plt.legend()
    # plt.xlabel('False Positive Rate')
    # plt.ylabel('True Positive Rate')
    # plt.savefig('rocs')
    # print('saved rocs')
    # plt.figure()
    # prs = np.array(curves)[scores['test_pr_curve']]
    # ps = []
    # mean_r = np.linspace(0, 1, 100)
    # plt.plot([0, 1], [sum(y)/len(y), sum(y)/len(y)], linestyle='--', lw=2, color='r', label='Chance', alpha=.8)
    # for p, r, _ in prs:
    #     plt.step(r, p, alpha=0.3, where='post')
    #     ps.append(scipy.interpolate.griddata(r, p, mean_r))
    # plt.plot(mean_r, np.mean(ps, axis=0), alpha=.8, label='Mean PR curve')
    # plt.legend()
    # plt.xlabel('Recall')
    # plt.ylabel('Precision')
    # plt.savefig('pr')
    # print('saved pr')

    for m, dataset in wv:
        X = np.stack(docs['vector_' + m + '_' + dataset].values)
        res = evaluate(X, y, np.log(docs['wc'].values), res, m)
        # model = MultinomialNB()
        # model = dummy.DummyClassifier()
        # avg precision = AU PRC

        X = np.concatenate((X_hp, np.stack(docs['vector_' + m + '_' + dataset].values)), axis=1)
        res = evaluate(X, y, np.log(docs['wc'].values), res, 'HP + ' + m)

res.to_pickle('res.pkl')
# if not USE_CACHED_DOCS:
# all_docs.to_pickle('docs.pkl', compression='gzip')

## res

results = res

results['AUROC'] = results['scores'].apply(lambda s: round(s['test_roc_auc'], 3))
results['AP'] = results['scores'].apply(lambda s: round(s['test_average_precision'], 3))
results['interval'] = results['scores'].apply(lambda s: np.round(s['interval'], 3))
results['wc'] = results['wc'].apply(lambda x: x if type(x) == np.str else str(max(x, 50)) + '-' + str(x + 1999))
range_res = results.loc[results['name'] == 'HP + FastText', ['wc', 'N', 'AUROC', 'AP']]

results['AUROC'] = results.apply(lambda x: '{AUROC} [{interval[0]}, {interval[1]}]'.format(**x), axis=1)
results = results.loc[(results['wc'] == 'all'), ['name', 'AUROC', 'AP']]

print()
print(range_res.to_latex(index=False))
print(results.to_latex(index=False))

print(results)
print(range_res)
