import numpy as np
import pandas as pd
import re

post_files = ["mini-normal1.json", "mini-normal2.json", "large-normal.json"]
# post_files = ["large-normal.json"]
slot_files = ["mini-normal-slots.json", "large-normal-slots.json", "old-normal-slots.json"]

posts = pd.concat(pd.read_json(fn, orient='records') for fn in post_files)
slots = pd.concat(pd.read_json(fn, orient='records') for fn in slot_files)

posts.set_index(['game_id', 'author'], inplace=True)

# remove post-game discussion

first_posts = posts[posts["post_no"] == 0]

# moderator makes the first post
mod_posts = posts.merge(first_posts, left_index=True, right_index=True, suffixes=('', '_first'))

# last vote count is the last game post
# everything after that is post-game discussion and should be discarded
vote_counts = mod_posts[mod_posts["content"].str.contains("vote ?count|vc|not voting \(", case=False)]
last_game_posts = vote_counts['post_no'].max(level=0)

game_posts = posts.join(last_game_posts, rsuffix='_last', how='inner')
game_posts = game_posts[game_posts['post_no'] <= game_posts['post_no_last']]

first_player_posts = game_posts[game_posts["post_no"] == 2]
# game_lengths = first_player_posts["inserted_at_last"] - first_player_posts["inserted_at"]
# print(game_lengths)
# print("average game length:", game_lengths.mean())

# concatenate all posts per player per game

post_elim_time = np.timedelta64(1, 'D')
groups = game_posts.sort_values('post_no').groupby(level=[0,1], sort=False)
# join all posts to a document; filter out the last 24h of each document
docs = groups.apply(lambda g: ' /!@ '.join(g[g['inserted_at'] < g['inserted_at'].max() - post_elim_time]['content'])).to_frame(name='content')
docs['inserted_at'] = groups['inserted_at'].min()
docs['updated_at'] = groups['inserted_at'].max()
docs.reset_index(inplace=True)

users = []

# slots = slots[slots['users'].map(len) == 1]
slots['scum'] = slots['role'].str.contains("mafia|goon|wolf|serial.?killer", case=False) | slots['role'].str.contains("SK")
slots['town'] = slots['role'].str.contains("town", case=False)
slots['sk'] = slots['role'].str.contains("serial.?killer", case=False) | slots['role'].str.contains("SK")
# use this when calculating game size/scum ratio correlations
# slots = slots.groupby('game_id').filter(lambda g: all(pd.notnull(g['scum'])))
slots = slots[~(slots['town'] & slots['scum']) & ~slots['sk']]
slots = slots.groupby('game_id').filter(lambda g: any(g['scum']))

slots['replacements'] = slots['users'].map(lambda x: len(x) - 1)
print(slots.groupby('scum').mean())


# slots.set_index('game_id', inplace=True)

slots.reset_index(inplace=True)
users = pd.DataFrame({'game_id': slot['game_id'], 'author': user, 'scum': slot['scum'], 'slot_id': i} for i, slot in slots.iterrows() for user in slot['users'])

docs = pd.merge(docs, users, on=['game_id','author'], how='inner')
docs['wc'] = docs['content'].apply(lambda c: len(re.findall(r"\w+", c.lower(), re.UNICODE)))
print(docs.groupby('scum').mean())
docs = docs[docs['wc'] >= 50]
print(docs['game_id'].nunique(), "games,", docs['slot_id'].nunique(), "slots,", len(docs), "documents")
print("scum ratio = ", sum(docs['scum']) / len(docs))

docs.to_pickle('docs.pkl', compression='gzip')
